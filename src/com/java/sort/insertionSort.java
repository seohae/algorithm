package com.java.sort;

import java.util.Arrays;

/**
 * 삽입정렬
 * 첫번째순서. 1번째 원소를 2번째 원소 ~ 1번째 원소까지의 원소와 비교하여 최소값을 해당 원소 자리로 이동
 * 두번째순서. 2번째 원소를 3번째 원소 ~ 1번째 원소까지의 원소와 비교하여 최소값을 해당 원소 자리로 이동
 * 세번째순서. 3번째 원소를 4번째 원소 ~ 1번째 원소까지의 원소와 비교하여 최소값을 해당 원소 자리로 이동
 */
public class insertionSort {
    public static void main(String[] args) {
        /** 1. 배열 선언 */
        int selectedArray[] = {5, 9, 3, 6, 2};
        System.out.println("정렬 전 > " + Arrays.toString(selectedArray));

        /** 반복문 > 배열 정렬 시작 */
        for (int i = 1; i < selectedArray.length; i++) {
            // i번째 원소를 저장
            int pivot = selectedArray[i];

            for (int j = i-1; j > -1; j--) {
                // i번째 원소와 이전 원소들 비교
                if (pivot < selectedArray[j]) {
                    // 큰 값을 그 다음 원소로 이동
                    selectedArray[j+1] = selectedArray[j];
                    // 이동된 원소 자리에 pivot 을 임시저장
                    selectedArray[j] = pivot;
                }
            }
        }

        System.out.println("정렬 후 > " + Arrays.toString(selectedArray));
    }
}
