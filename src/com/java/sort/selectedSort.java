package com.java.sort;

import java.util.Arrays;

/**
 * 선택정렬
 * 첫번째순서. 1번째 원소를 2번째 원소 ~ 마지막 원소까지의 원소와 비교하여 최소값을 1번째 자리로 이동
 * 두번째순서. 2번째 원소를 3번째 원소 ~ 마지막 원소까지의 원소와 비교하여 최소값을 2번째 자리로 이동
 * 세번째순서. 3번째 원소를 4번째 원소 ~ 마지막 원소까지의 원소와 비교하여 최소값을 3번째 자리로 이동
 */
public class selectedSort {
    public static void main(String[] args) {
        /** 1. 배열 선언 */
        int selectedArray[] = {5, 9, 3, 6, 2};
        System.out.println("정렬 전 > " + Arrays.toString(selectedArray));

        /** 반복문 > 배열 정렬 시작 */
        for (int i = 0; i < selectedArray.length; i++) {
            // i번째 원소를 저장
            int min = selectedArray[i];

            for (int j = i+1; j < selectedArray.length; j++) {
                // i번째 원소와 그 다음 원소 ~ 마지막 원소 비교
                if (selectedArray[j] < min) {
                    /** 해당 반복문을 돌다보면 selectedArray[i] 가 계속 바뀌므로, temp 임시 저장이 필요하다. */
                    // 현재 i번째 원소를 temp 변수에 임시 저장
                    int temp = selectedArray[i];
                    // 최소값 변경
                    min = selectedArray[j];
                    // i번째 원소에 비교된 작은 값으로 대체
                    selectedArray[i] = selectedArray[j];
                    // 대체된 원소 자리에 i번째 원소를 저장
                    selectedArray[j] = temp;
                }
            }
        }

        System.out.println("정렬 후 > " + Arrays.toString(selectedArray));
    }
}
